
let course = {
	id : '01',
	name : 'Algorithm',
	price : '1,000',
	isActive : true
};

let courseArr = [];

let postCourse = newCourse => {
	courseArr.push(newCourse);
	console.log(`You have created ${newCourse.name} course. The price is ${newCourse.price}`);
}

postCourse(course);

const findCourseID = courseArr.find(({id}) => id === '01');
console.log(findCourseID);

const deleteCourse = courseArr.pop();
console.log(courseArr);
